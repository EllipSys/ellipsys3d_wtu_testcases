#----------------------------
#         mann.py
#----------------------------
# Short script using the pymann2fparser tool
# to generate a mann turbulence box.
#----------------------------

from pymann.main import pymann
import numpy as np

# reference lenght scale, equal to NREL5MW radius
R = 63
#----Mann turbulence box inputs----
# n: number of cells in (flow | cross-flow | vertical) directions
n = np.array([1024, 64, 64])
# box side lengths (outer boundaries)
Lbox = np.array([160*R, 10*R, 10*R])
# U: mean wind speed [m/s], which determines the total time interval for which the turbulence
#    exists ie. T = box_length/U.
U = 8.
# parameters should lead to TI of about 4.5% 
pym = pymann(ae=0.02,l=40,Lbox=Lbox, n=n, U=U)
# generate turbulence
pym.update_turb()
