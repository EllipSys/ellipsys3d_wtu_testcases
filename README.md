# ellipsys3d_wtu_testcases

Testcases for WindTurb - the method for applying velocity fluctuations over a plane inside a computational domain by [Troldborg et al.](https://doi.org/10.1002/we.1608) - and an equivalent method `inlet_fparser` for applying fparser velocity files at the inlet boundary. Velocities are superimposed on the underlying flowfield, at the inlet it can also replace the existing conditions.

The verification of these methods and their equivalence is detailed in the document "Turbulent_inflow_verification.pdf".


## Test case description

There are two testcases:

- `run/`: Standard fparser inflow with a WindTurb turbulence plane

- `run_inflow/`: Function fparser inflow with a WindTurb turbulence plane and an `inlet_fparser`


## WindTurb command line inputs

![wtu_definition](wtu_definition.png)

The figure above tries to visualize the assumed input format and some of the different user inputs that can be specified. It is assumed that the user will provide the velocities in the plane coordinate system, **not** the computational domain coordinate system. The plane and its velocity component are transformed in the domain coordinate system according to the positioning and direction vectors of the plane, specified by the user. Inputs are assumed to be vertex-based and have their origin in the right corner.

| Input line | Type | Description |
| ------     | ---- | ------- |
| windturb | - | Invokes the use of the WindTurb model, all inputs should follow this statement, their order is not important. |
| fparser_wtu | Text(3) | Filenames of the fparser files of each velocity component - order is cross, vertical and plane normal u_[1,2,3]. |
| rotcentre_wtu | Real(3) | Define the rotational centre of the fparser input, assumed is the right corner to be (0,0,0), this is also the default. |
| pos_wtu | Real(4) | Position the rotational centre in the domain (x,y,z) and the final real is the plane normal coordinate at which to extract the reference wind speed for calculating the body forces. It is defined in the local plane coordinate system, so negative is upstream if dir_wtu is aligned with the mean flow direction. |
| dir_wtu | Real(3) | Plane normal vector, e_3 |
| sdir_wtu | Real(3) | Plane cross vector, e_1 |
| scale_wtu | Real(3) | scaling of the velocity components, cross, vertical and plane normal |
| smear_wtu | Real(1) | Plane-normal smearing length scale used in Gaussian kernel for force distribution in computational domain, that triggers velocity fluctuations. Should be 2-3 times the grid size for stability reasons. |
| tstart_wtu | Real(1) |  Time instance, when to start injecting velocities |
| tshift_wtu | Real(1) |  Time shift fparser input fields. If negative it starts from the back. Input is recycled. |


## inlet_fparser command line inputs

This method is conceptually identical to the WindTurb model and thus also inherits the command line input definitions from it, however velocities are applied over boundary faces within a specified attribute range. Therefore the naming is close to the usual method for applying fparser inflow at the inlet.

| Input line | Type | Description |
| ------     | ---- | ------- |
| inlet_fparser | Integer(2), Text(3), ([ , sum]) | Invokes the use of the WindTurb way of handling velocities. First two inputs specify the attribute range over which to apply the velocites, followed by the filenames of the fparser files of each velocity component - order is cross, vertical and plane normal u_[1,2,3]. Final input is an optinal statement, if nothing is specified than existing boundary conditions are replaced in the spatial region for which the input files hold data, "sum" will add the velocities to existing conditions. |
| inlet_modfile pos | Real(3) | Identical to pos_wtu, except no plane normal distance from the plane needs to be specified, as no reference velocity is needed. One can also leave one position out, if the plane overlaps with the inlet plane. |
| inlet_modfile rotcentre |  | All other inputs are identical to the WindTurb one, just preceded by the statement "inlet modfile" and removing the subscript "_wtu".|
|  : | :  | : |